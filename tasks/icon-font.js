'use strict';

/*
 * Create icon font + css
 *
 * ./tasks/icon-font.js
 */
module.exports = function (gulp, $, config) {
    var iconfont = require('gulp-iconfont'),
        consolidate = require('gulp-consolidate'),
        runTimestamp = Math.round(Date.now() / 1000);

    // return task
    return {

        // the task name
        'icon-font': {

            // the task function
            fn: createIconFont,

            // task dependencies
            //dep : ['dep-1', 'dep-2'],

            // task sequence using run-sequence
            //seq: ['sass-lint'],

            // wether it is the default task
            isDefault: false,

            // help description
            description: 'Create icon fonts from different sources & create a icon stylesheet'
        }
    };


    function createIconFont(done) {

        gulp.src(config.options.icons.src)
            .pipe(iconfont({
                //appendUnicode: true,
                fontName: config.options.icons.fontName,
                formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'],
                timestamp: runTimestamp
            }))
            .on('glyphs', function (glyphs, options) {
                gulp.src(config.options.icons.srcCSS)
                    .pipe(consolidate('lodash', {
                        glyphs: glyphs,
                        fontName: config.options.icons.fontName,
                        fontPath: './', // set path to font (from your CSS file if relative)
                        className: config.options.icons.className

                    }))
                    .pipe(gulp.dest(config.options.folders.dist + config.options.icons.dest))
                ;
            })
            .pipe(gulp.dest(config.options.folders.dist + config.options.icons.dest))
        ;
        done();

    }
};