'use strict';

/*
 * Linting SCSS files
 *
 * ./tasks/sass-lint.js
 */
module.exports = function (gulp, $, config) {
    var scsslint = require('gulp-scss-lint');

    // return task
    return {

        // the task name
        'sass-lint': {

            // the task function
            fn: sassLintTask,

            // task dependencies
            //dep : ['dep-1', 'dep-2'],

            // task sequence using run-sequence
            //seq : ['dep-3', ['dep-4', 'dep-5']],

            // wether it is the default task
            isDefault: false,

            // help description
            description: 'Linting SCSS files'
        }
    };


    function sassLintTask(done) {

        // The base is needed for the sourcemaps to work.
        gulp.src(config.options.styles.sassSrc)
            .pipe(scsslint({
                'config': 'node_modules/whoeii-tooling/sass-lint.yml',
                'filePipeOutput': 'scssReport.json'
            }))
            .pipe(gulp.dest('./reports'));
        done();

    }
};