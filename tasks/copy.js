'use strict';

/*
 * Copy files to different folders
 *
 * ./tasks/copy.js
 */
module.exports = function (gulp, $, config) {

    // return task
    return {

        // the task name
        'copy': {

            // the task function
            fn: copyTask,

            // task dependencies
            //dep : ['dep-1', 'dep-2'],

            // task sequence using run-sequence
            //seq : ['dep-3', ['dep-4', 'dep-5']],

            // wether it is the default task
            isDefault: false,

            // help description
            description: 'Copy libs, fonts & html'
        }
    };


    function copyTask(done) {

        // copy files to "distribution folder" + optional destination
        if (config.options.files.toCopyFiles.src.length > 0) {
            gulp.src(config.options.files.toCopyFiles.src)
                .pipe(gulp.dest(config.options.folders.dist + config.options.files.toCopyFiles.dest))
                .on('error', errorlog);

        }

        // copy libs to "distribution folder" + optional destination
        if (config.options.files.toCopyLibs.src.length > 0) {
            gulp.src(config.options.files.toCopyLibs.src)
                .pipe(gulp.dest(config.options.folders.dist + config.options.files.toCopyLibs.dest))
                .on('error', errorlog);

        }

        // copy HTML files
        gulp.src('src/**/*.html')
            .pipe(gulp.dest(config.options.folders.dist))
            .on('error', errorlog);


        // copy fonts to "distribution folder" + optional destination
        if (config.options.files.fonts.src.length > 0) {
            gulp.src(config.options.files.fonts.src)
                .pipe(gulp.dest(config.options.folders.dist + config.options.files.fonts.dest))
                .on('error', errorlog);

        }

        done();

    }

    // error, without breaking flow.
    function errorlog(err) {
        console.log(err.message);
        this.emit('end');
    }
};