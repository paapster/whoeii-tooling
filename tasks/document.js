'use strict';

/*
 * Create CSS files from different sources & themes
 *
 * ./tasks/stylesheets.js
 */
module.exports = function (gulp, $, config) {
    var fs = require('fs');
    var path = require('path');
    var handlebars = require('gulp-compile-handlebars');
    var rename = require('gulp-rename');


    var handelbarsHelpers = {
        ifCond: function (v1, operator, v2, options) {
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '!=':
                    return (v1 != v2) ? options.fn(this) : options.inverse(this);
                case '!==':
                    return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        },
    };



    // return task
    return {

        // the task name
        'document': {

            // the task function
            fn: documentTask,

            // task dependencies
            //dep : ['dep-1', 'dep-2'],

            // task sequence using run-sequence
            //seq : ['clean'], //, ['dep-4', 'dep-5']],

            // wether it is the default task
            isDefault: false,

            // help description
            description: 'Create documentation demo site'
        }
    };


    function documentTask(done) {

        function getDirectories(srcpath, cb) {
          return fs.readdirSync(srcpath).filter(function(file) {
            return fs.statSync(path.join(srcpath, file)).isDirectory();
          });
        }

        var options = {
            ignorePartials: true,
            partials : {},
            batch : [],
            helpers : handelbarsHelpers
        }

        var types = getDirectories('./src/docs/');
        var folders = {};
        for (var i = 0; i < types.length; i++) {
            folders[types[i]] = getDirectories('./src/docs/' + types[i] + '/');
        }

        var componentlist = {componentlist: folders}
        gulp.src('./src/index.hbs')
            .pipe(handlebars(componentlist, options))
            .pipe(rename('index.html'))
            .pipe(gulp.dest('./src'));
        done();

    };

};