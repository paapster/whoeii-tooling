/*
 * loading configuration(s) and merge them into one set of options
 */
module.exports = function getOptions() {
    var fs = require('fs'),
        path = require('path'),
        whoeiiLocalConfig,
        whoeiiDefaultConfig,
        merged,

    // Set local root (/node_modules/whoeii-tooling/tasks/_tooling/)
        dir = path.join(__dirname, '../', '../', '../', '../');

    // check isObject
    function isObject(obj) {
        return Object.prototype.toString.call(obj) === "[object Object]";
    }

    // Merge
    function merge(a, b) {
        Object.keys(b).forEach(function (prop) {
            if (isObject(a[prop]) && isObject(b[prop])) {
                a[prop] = merge(a[prop], b[prop]);
            }
            else {
                a[prop] = b[prop];
            }
        });

        return a;
    }

    //Kaskade objects
    function kaskade(dest) {
        var sources = [].slice.call(arguments, 1);

        sources.forEach(function (source) {
            dest = merge(dest, source);
        });

        return dest;
    }

    /*
     * load default configuration (./tasks/_tooling/)
     */
    try {
        whoeiiDefaultConfig = JSON.parse(fs.readFileSync(path.join(__dirname, '../', '../', 'gulp.default.config.json'), 'utf-8'));
    } catch (error) {
        if (error.code === 'ENOENT') {
            console.log('The file gulp.default.config.json not found!');
        } else {
            throw error;
        }
    }

    /*
     * Loop to the local directory, filter on config json files
     */
    try {
        files = fs.readdirSync(dir).filter(function (items) {
            return items.indexOf('gulp.local.config.json') !== -1;
        });
        if (files !== 'undefned') {
            try {
                // get local project config
                whoeiiLocalConfig = JSON.parse(fs.readFileSync(dir + files[0], 'utf-8'));
            } catch (error) {
                if (error.code === 'ENOENT') {
                    console.log('The file gulp.local.config.json not found!');
                    try {
                        // get backup local config
                        whoeiiLocalConfig = JSON.parse(fs.readFileSync(path.join(__dirname, '../', '../', 'gulp.local.config.json'), 'utf-8'));
                        console.error('No project "gulp.local.config.json", found in ' + dir + ', using default config');
                    } catch (error) {
                        if (error.code === 'ENOENT') {
                            console.log('No configuration file was found at all');
                        } else {
                            throw error;
                        }
                    }
                } else {
                    throw error;
                }
            }
        }
    } catch (error) {
        if (error.code === 'ENOENT') {
            console.log('Directory ' + dir + ' not found!');
        } else {
            throw error;
        }
    }

    // Get both config files & merge them
    merged = kaskade(whoeiiDefaultConfig, whoeiiLocalConfig, {'merged-version': '0.0.1'});

    // returm merged result
    //console.log(JSON.stringify(merged, null, 4));
    return merged;

};
