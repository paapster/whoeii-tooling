'use strict';

/*
 * Watch task
 *
 * ./tasks/watch.js
 */
module.exports = function (gulp) {

    // return task
    return {

        // the task name
        watch: {

            // the task function
            fn: watchTask,

            // task dependencies
            dep : ['document', 'webserver'],

            // task sequence using run-sequence
            //seq : ['dep-3', ['dep-4', 'dep-5']],
            //seq : [],

            // wether it is the default task
            isDefault : false,


            // help description
            description: 'Watching for changes in predefined files & folders'
        }
    };


    function watchTask() {
        //gulp.watch('src/js/**/*.js', ['build-src']);
        gulp.watch('src/**/*.scss', ['stylesheets']);
        gulp.watch('src/**/*.svg', ['icon-font']);
        gulp.watch('src/**/*.html', ['document']);
        gulp.watch('src/**/*.hbs', ['document']);
        gulp.watch('./package.json', function (event) {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        });

    }
};
