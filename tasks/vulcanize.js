'use strict';

/*
 * Linting SCSS files
 *
 * ./tasks/sass-lint.js
 */
module.exports = function (gulp, $, config) {
    var vulcanize = require('gulp-vulcanize');
    var rename = require('gulp-rename');

    // return task
    return {

        // the task name
        'vulcanize': {

            // the task function
            fn: vulcanizeTask,

            // task dependencies
            dep : ['stylesheets', 'document'],

            // task sequence using run-sequence
            //seq : ['dep-3', ['dep-4', 'dep-5']],

            // wether it is the default task
            isDefault: false,

            // help description
            description: 'packing webcomponents into one file'
        }
    };


    function vulcanizeTask(done) {

        // Build all polymer elemnts into one
        gulp.src('src/components/elements.vulcanized.html')
            .pipe(vulcanize({
                abspath: ''
            })).on( "error", function( err ) {
              console.log( err );
            })
            .pipe(rename('components.html'))
            .pipe(gulp.dest(config.options.folders.dist));

        // Copy all the theme styles into the dist
        gulp.src(['src/components/distcss/*'])
            .pipe(gulp.dest(config.options.folders.dist + 'distcss/'));

        done();
    }
};