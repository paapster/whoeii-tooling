'use strict';

/*
 * Start the development webserver
 *
 * ./tasks/webserver.js
 */
module.exports = function(gulp, $, config) {
    var connect = require('gulp-connect');

    // return task
    return {

        // the task name
        'webserver': {

            // the task function
            fn: webserverTask,

            // task dependencies
            //dep : ['dep-1', 'dep-2'],

            // task sequence using run-sequence
            //seq : ['dep-3', ['dep-4', 'dep-5']],

            // wether it is the default task
            isDefault: false,

            // help description
            description: 'Run the mock server for development purpouse'
        }
    };

    function webserverTask() {
        connect.server({
            root: config.options.default.env.dev.root,
            port: config.options.default.env.dev.port,
            livereload: config.options.default.env.dev.livereload
        });
    }
};