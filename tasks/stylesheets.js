'use strict';

/*
 * Create CSS files from different sources & themes
 *
 * ./tasks/stylesheets.js
 */
module.exports = function (gulp, $, config) {
    var argv = require('yargs').argv,
        autoprefixer = require('gulp-autoprefixer'),
        concat = require('gulp-concat'),
        gulpIf = require('gulp-if'),
        cleanCSS = require('gulp-clean-css'),
        sass = require('gulp-sass'),
        sourcemaps = require('gulp-sourcemaps');

    var glob = require("glob");
    var fs = require('fs');
    var _ = require("lodash");


    // return task
    return {

        // the task name
        'stylesheets': {

            // the task function
            fn: compileStylesTask,

            // task dependencies
            //dep : ['dep-1', 'dep-2'],

            // task sequence using run-sequence
            seq: ['sass-lint'],

            // wether it is the default task
            isDefault: false,

            // help description
            description: 'Create CSS files from different sources & themes'
        }
    };


    function compileStylesTask(done) {


        var getUniqueThemes = function (files) {
            var filenames = _.map(files, function (path) {
                return path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));
            });
            return _.uniq(filenames);
        };

        var getUniqueFolders = function (files) {
            var paths = _.map(files, function (path) {
                return path.substring(0, path.lastIndexOf("/") + 1);
            });
            return _.uniq(paths);
        };

        // The base is needed for the sourcemaps to work.
        gulp.src(config.options.styles.sassSrc, {base: './'})

            // Sourcemaps only for development.
            .pipe(gulpIf(!argv.production, sourcemaps.init()))
            .pipe(sass().on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['> 1%', 'last 3 versions']
            }))
            .pipe(gulp.dest('./'));


            setTimeout(function(){
                glob(config.options.styles.cssSrc[0], function (er, files) {

                    var uniqueThemes = getUniqueThemes(files);
                    var uniqueFolders = getUniqueFolders(files);

                    _.forEach(uniqueThemes, function(value, key) {
                        var themeName = value;
                        var filelist = [];

                        _.forEach(uniqueFolders, function(folderName) {
                            if (fs.existsSync(folderName + themeName + '.css')) {
                                filelist.push(folderName + themeName + '.css');
                            } else {
                                filelist.push(folderName + 'default.css');
                            }
                        });

                         gulp.src(filelist, {base: './'})
                            .pipe(concat(themeName + '.css'))
                            .pipe(gulp.dest('./src/components/distcss/'));

                    });
                    done();
                });
            }, 100);



    }
};