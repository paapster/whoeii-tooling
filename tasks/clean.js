'use strict';

/*
 * Clean the distribtion & build.
 *
 * ./tasks/clean.js
 */
module.exports = function (gulp, $, config) {
    var del = require('del');

    // return task
    return {

        // the task name
        'clean': {

            // the task function
            fn: cleanTask,

            // task dependencies
            //dep : ['dep-1', 'dep-2'],

            // task sequence using run-sequence
            //seq : ['dep-3', ['dep-4', 'dep-5']],

            // wether it is the default task
            isDefault: false,

            // help description
            description: 'Clean folders'
        }
    };

    function cleanTask(done) {
        del([config.options.folders.dist, config.options.folders.temp]);

        done();
    }
};
