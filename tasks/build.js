'use strict';

/*
 * Build src
 *
 * ./tasks/build.js
 */
module.exports = function (gulp) {


    // return task
    return {

        // the task name
        build: {
            // the task function
            fn: buildSrc,

            // task dependencies
            //dep : ['dep-1', 'dep-2'],

            // task sequence using run-sequence
            seq : ['clean', 'copy', ['icon-font', 'stylesheets']],

            // wether it is the default task
            isDefault: false,

            // help description
            description: 'Build source'
        }
    };


    function buildSrc(done) {

        done();
    }
};
