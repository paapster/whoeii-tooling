#Whoeii Tooling
##One configurable tooling usable in multiple projects

The whoeii tooling is the one shared tooling to use across the multiple projects. All features will be build with this tooling

## Prerequisites
**Icon-font**
requires that ttfautohint is installed for transforming svg fonts into usable fonts.
**SCSS-lint**
requires that the scss_lint gem is installed.
```
$sudo gem install scss_lint
```



## Preferred usage for development:
