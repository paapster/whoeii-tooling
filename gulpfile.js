"use strict";

var gulp = require('gulp'),

/* get all options */
    getOptions = require('./tasks/_tooling/_getLocalConfig'),
    options = getOptions(),

/* load npm package */
    whoeiiToolingPackage = require('./package.json'),

/* load plugins */
    gulpLoadPlugins = require('gulp-load-plugins'),
    taskLoader = require('gulp-commonjs-tasks/task-loader'),
    plugins = gulpLoadPlugins();


/*
 * loadig tasks from the tasks folder
 */
var tasksContext = taskLoader.load(
    __dirname + '/tasks',
    gulp,
    plugins, {
        options: options,
        package: whoeiiToolingPackage,
        dependencies: Object.keys(whoeiiToolingPackage && whoeiiToolingPackage.dependencies || {}),
        dependenciesInSource: [],
        peerDependencies: []
    }
);


tasksContext.addHelpTask();
